﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace NavigationApp1
{
    public partial class NavigationApp1Page : ContentPage
    {
        public NavigationApp1Page()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavigationApp1Page)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        async void OnWhaleSharkTapped(object sender, System.EventArgs e)
        {
          await Navigation.PushAsync(new WhaleSharks());
        }

        async void OnPassTextTapped(object sender, System.EventArgs e)
        {
          
            await Navigation.PushAsync(new Monkeys("This is a white-headed capuchin"));

           
       }

      
    }
}