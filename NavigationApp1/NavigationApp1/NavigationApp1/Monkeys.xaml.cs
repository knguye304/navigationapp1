﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NavigationApp1
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Monkeys : ContentPage
    {

	public Monkeys (string nameOfMonkey)
		{
            InitializeComponent();
            MonkeyText.Text = nameOfMonkey;
		}

     
	}
}